package com.jdbrewer.hgwotd.utils

import android.content.Context
import java.util.*



class Prefs {
    val PREFS_FILENAME = "com.jdbrewer.hgwotd.prefs"
    val CURRENT_DATE = "current_date"
    val timeMillis = Calendar.getInstance().timeInMillis
}