package com.jdbrewer.hgwotd.utils

import android.content.Context
import com.jdbrewer.hgwotd.models.Word
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException



object WordHelper {

    val KEY_WORD = "Hebrew"
    val KEY_DEF = "English"
    val KEY_OCCUR = "occurrences"


    fun getWordsFromJson(fileName: String, context: Context): ArrayList<Word> {

        val words = ArrayList<Word>()
            try {
                val jsonString = loadJsonFromFile(fileName, context)
                val json = JSONObject(jsonString)
                val jsonWords = json.getJSONArray("words")

                for (index in 0 until jsonWords.length()) {
                    val wordWord = jsonWords.getJSONObject(index).getString(KEY_WORD)
                    val wordDef = jsonWords.getJSONObject(index).getString(KEY_DEF)
//                    val wordOccur = jsonWords.getJSONObject(index).getInt(KEY_OCCUR)

                    words.add(Word(wordWord, wordDef))
                }
            } catch (e: JSONException) {
                return words
            }
            return words

    }

    private fun loadJsonFromFile(filename: String, context: Context): String {
        var json = ""
        try {
            val input = context.assets.open(filename)
            val size = input.available()
            val buffer = ByteArray(size)
            input.read(buffer)
            input.close()
            json = buffer.toString(Charsets.UTF_8)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return json
    }
}