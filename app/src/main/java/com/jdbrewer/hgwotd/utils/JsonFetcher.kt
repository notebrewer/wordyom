package com.jdbrewer.hgwotd.utils

import android.content.Context
import android.util.Log
import android.widget.Toast
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException



class JSONFetcher(val context: Context) {

    private val type = "hebrew.json"

    fun fetch(): JSONObject? {

        try {
            val json = jsonToStringFromAssetFolder(type, context)
            return JSONObject(json)
        } catch (e: IOException) {
            Toast.makeText(context, "Error finding File", Toast.LENGTH_SHORT).show()
            Log.e("JSONFetcher", e.message)
            return null
        } catch (e: JSONException) {
            Toast.makeText(context, "Error parsing JSONObject", Toast.LENGTH_SHORT).show()
            Log.e("JSONFetcher", e.message)
            return null
        }

    }

    companion object {
        @Throws(IOException::class)
        fun jsonToStringFromAssetFolder(fileName: String, context: Context): String {
                val manager = context.assets
                val file = manager.open(fileName)

                val data = ByteArray(file.available())
                file.read(data)
                file.close()

            return String(data)
        }
    }
}

