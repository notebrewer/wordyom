package com.jdbrewer.hgwotd.utils

import android.content.Context



object DateHelper {

    val PREFS_FILENAME = "com.jdbrewer.hgwotd.prefs"
    val LAUNCH_DAY = "launch_day"

    fun hasAppBeenLaunched(context: Context): Boolean {
        when {
            exists(context, LAUNCH_DAY) -> return true
            else -> return getLaunchDay(context) != 0L
        }
    }

    fun removeStartTime(context: Context) {
        remove(context, LAUNCH_DAY)
    }

    fun getLaunchDay(context: Context): Long {
        return getLong(context, LAUNCH_DAY, 0)
    }

    fun setLaunchDay(context: Context, time: Long) {
        putLong(context, LAUNCH_DAY, time)
    }

    fun setSharedPreferenceInt(context: Context, key: String, value: Int) {
        val settings = context.getSharedPreferences(PREFS_FILENAME, 0)
        val editor = settings.edit()
        editor.putInt(key, value)
        editor.apply()
        editor.commit()
    }

    fun getSharedPreferenceInt(context: Context, key: String, value: Int): Int {
        val settings = context.getSharedPreferences(PREFS_FILENAME, 0)
        return settings.getInt(key, value)
    }

}