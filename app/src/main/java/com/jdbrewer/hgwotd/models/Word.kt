package com.jdbrewer.hgwotd.models



data class Word(val Hebrew: String, val English: String)