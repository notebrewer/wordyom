package com.jdbrewer.hgwotd.fragments



import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jdbrewer.hgwotd.R
import com.jdbrewer.hgwotd.models.Word
import com.jdbrewer.hgwotd.utils.WordHelper



class WordFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState:
    Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_word, container, false)
        val wordTextView = view.findViewById<TextView>(R.id.wordTextView)
        val defTextView = view.findViewById<TextView>(R.id.defTextView)
        // val occurTextView = view.findViewById<TextView>(R.id.occurTextView)
        // val dateTextView = view.findViewById<TextView>(R.id.dateTextView)

        val args = arguments
        wordTextView.text = args.getString(WordHelper.KEY_WORD)
        defTextView.text = args.getString(WordHelper.KEY_DEF)
        // occurTextView.text = String.format("%d", args.getInt(WordHelper.KEY_OCCUR))
        // dateTextView.text = DateHelper.getSharedPreferenceInt(context, "current_day_year", defValue = 0).toString()

        return view

    }

    companion object {

        fun newInstance(word: Word): WordFragment {

            val args = Bundle()
            args.putString(WordHelper.KEY_WORD, word.Hebrew)
            args.putString(WordHelper.KEY_DEF, word.English)
            // args.putInt(WordHelper.KEY_OCCUR, word.occurrences)
            // args.putString(WordHelper.KEY_DEF, word.definition)
            // args.putInt(WordHelper.KEY_OCCUR, word.occurrences)

            val fragment = WordFragment()
            fragment.arguments = args
            return fragment
        }
    }

}
