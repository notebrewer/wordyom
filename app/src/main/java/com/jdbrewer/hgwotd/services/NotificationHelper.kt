package com.jdbrewer.hgwotd.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.view.ViewPager
import com.jdbrewer.hgwotd.activities.MainActivity
import com.jdbrewer.hgwotd.R
import com.jdbrewer.hgwotd.adapters.WordPagerAdapter



internal class NotificationHelper

(ctx: Context): ContextWrapper(ctx) {

    private
    var manager: NotificationManager ? = null
    private val smallIcon: Int
        get() = android.R.drawable.ic_menu_info_details
    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val chan1 = NotificationChannel(PRIMARY_CHANNEL, getString(R.string.noti_channel_default), NotificationManager.IMPORTANCE_DEFAULT)
            chan1.lightColor = Color.GREEN
            chan1.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
            getManager().createNotificationChannel(chan1)
        }
    }
    private lateinit
    var pagerAdapter: WordPagerAdapter
    private lateinit
    var viewPager: ViewPager
    val myintent = Intent(this, MainActivity::class.java)
    val contentIntent = PendingIntent.getActivity(this, 0,
            myintent, PendingIntent.FLAG_UPDATE_CURRENT)



    fun getNotification1(title: String, body: String): NotificationCompat.Builder ? {
        return NotificationCompat.Builder(applicationContext, PRIMARY_CHANNEL)
                .setContentIntent(contentIntent)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(smallIcon)
                .setAutoCancel(true)
                .setChannelId(PRIMARY_CHANNEL)
    }

    fun notify(id: Int, notification: Notification.Builder) {
        getManager().notify(id, notification.build())
    }

    fun getManager(): NotificationManager {
        if (manager == null) {
            manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }
        return manager as NotificationManager
    }

    companion object {
        val PRIMARY_CHANNEL = "default"
    }
}