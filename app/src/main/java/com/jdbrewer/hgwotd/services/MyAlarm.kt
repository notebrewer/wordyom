package com.jdbrewer.hgwotd.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.jdbrewer.hgwotd.R
import com.jdbrewer.hgwotd.services.NotificationHelper



class MyAlarm : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        Log.d("MyAlarm", "YOUR ALARM WAS TRIGGERED")
        val notificationHelper = NotificationHelper(context)
        var nb: NotificationCompat.Builder? = notificationHelper.getNotification1(context.getString(R.string.main_primary_title), context.getString(R.string.primary1_body))
        notificationHelper.getManager().notify(1, nb?.build())

    }
}