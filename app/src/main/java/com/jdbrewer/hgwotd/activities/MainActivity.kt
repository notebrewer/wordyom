package com.jdbrewer.hgwotd.activities

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import android.widget.ViewFlipper
import com.nshmura.recyclertablayout.RecyclerTabLayout
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList
import android.content.DialogInterface
import android.graphics.Color
import android.os.Handler
import android.webkit.WebView
import com.jdbrewer.hgwotd.BuildConfig
import com.jdbrewer.hgwotd.R
import com.jdbrewer.hgwotd.adapters.WordListAdapter
import com.jdbrewer.hgwotd.adapters.WordPagerAdapter
import com.jdbrewer.hgwotd.services.MyAlarm
import com.jdbrewer.hgwotd.services.NotificationHelper
import com.jdbrewer.hgwotd.utils.*



class MainActivity : AppCompatActivity() {

    private lateinit var wordListAdapter: WordListAdapter
    private lateinit var pagerAdapter: WordPagerAdapter
    private lateinit var viewPager: ViewPager
    private lateinit var recyclerTabLayout: RecyclerTabLayout
    private var noti: NotificationHelper? = null
    private var pendingIntent: PendingIntent? = null
    internal var destroyed = false

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkFirstRun()
        noti = NotificationHelper(this)

        val cal = Calendar.getInstance()
        val doy = cal[Calendar.DAY_OF_YEAR]
        val launchDate = DateHelper.getSharedPreferenceInt(baseContext, "launch_date", -1)
        val wordIndex = (doy - launchDate) + 1
        val words = WordHelper.getWordsFromJson("hebrew.json", this)
        val wordSet = words.subList(0, wordIndex)
        val wordSetArrayList = ArrayList(wordSet)
        val recyclerView = findViewById<RecyclerView>(R.id.wordList)
        val position: Int
        val extras = intent.extras

        viewPager = findViewById(R.id.viewPager)
        pagerAdapter = WordPagerAdapter(supportFragmentManager, wordSetArrayList, wordIndex)
        viewPager.adapter = pagerAdapter

        if (extras != null && savedInstanceState == null) {
            position = extras.getInt("viewpager_position")

        } else if (savedInstanceState != null) {
            position = savedInstanceState.getInt("viewpager_position")
        } else {
            position = wordSetArrayList.size
        }

        viewPager.currentItem = position
        viewPager.setPageTransformer(true, DepthPageTransformer())
        wordListAdapter = WordListAdapter(wordSetArrayList)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        recyclerView.adapter = wordListAdapter
        recyclerTabLayout = findViewById(R.id.recyclerTabLayout)
        recyclerTabLayout.setUpWithViewPager(viewPager)
        setAlarm(this)
    }

    private fun checkFirstRun() {

        val PREFS_NAME = "MyPrefsFile"
        val PREF_VERSION_CODE_KEY = "version_code"
        val DOESNT_EXIST = -1
        val currentVersionCode = BuildConfig.VERSION_CODE
        val prefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST)

        if (currentVersionCode == savedVersionCode) {
            return

        } else if (savedVersionCode == DOESNT_EXIST) {
            DateHelper.setSharedPreferenceInt(baseContext, "launch_date", Calendar.getInstance()[Calendar.DAY_OF_YEAR])

        } else if (currentVersionCode > savedVersionCode) {

        }

        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply()
    }

    inline fun doAsync(crossinline f: () -> Unit) {
        Thread({
            f()
        }).start()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)
        return true
    }

    inline fun consume(f: () -> Unit): Boolean {
        f()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) =

            when (item.itemId) {
                R.id.wordListMenu -> consume {
                    val vf = findViewById<View>(R.id.viewFlipper) as ViewFlipper
                    vf.displayedChild = vf.indexOfChild(wordListLayout)
                }
                R.id.viewPagerMenu -> consume {
                    val vf = findViewById<View>(R.id.viewFlipper) as ViewFlipper
                    vf.displayedChild = vf.indexOfChild(viewPagerLayout)
                }
                R.id.aboutMenuItem -> consume {
                    aboutDialog()
                }
                else -> super.onOptionsItemSelected(item)
            }

    var doubleBackToExitPressedOnce = false

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        } else {

            super.onBackPressed()
        }

        this.doubleBackToExitPressedOnce = true

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }

    override fun onDestroy() {
        super.onDestroy()
        destroyed = true

        if (intent.extras != null) {
            try {
                intent.extras.remove("viewpager_position")
            } catch (e: SecurityException) {
                e.printStackTrace()
            }

        }
    }

    fun cancel() {
        val manager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        manager.cancel(pendingIntent)
        Toast.makeText(this, "Alarm Canceled", Toast.LENGTH_SHORT).show()
    }

    private fun setAlarm(context: Context) {

        val am = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, MyAlarm::class.java)
        intent.putExtra("one_time", java.lang.Boolean.FALSE)
        val pi = PendingIntent.getBroadcast(context, 0, intent, 0)
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()

        if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) >= 9) {
            Log.d("IF SET", "Alarm will schedule for next day!")
            calendar.add(Calendar.DAY_OF_YEAR, 1) // add, not set!
        } else {
            Log.d("ELSE SET", "Alarm will schedule for today!")
        }
        calendar.set(Calendar.HOUR_OF_DAY, 9)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)

        am.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.timeInMillis,
                AlarmManager.INTERVAL_DAY, pi)
    }

    @SuppressLint("RestrictedApi")
    private fun aboutDialog() {
        val alert = android.support.v7.app.AlertDialog.Builder(this)
        alert.setTitle("")
        val webView = WebView(this)
        var about =
                "<h1>HG WOTD - 0.1 alpha</h1>" +
                        "<p>A simple flashcard app for learning Hebrew words.</p>" +
                        "<p>Developed by <a href='mailto:t.notebrewer@gmail.com'>Jonathan Brewer</a></p>" +
                        "<p>Cardo Font provided by <a href='http://www.scholarsfonts.net/cardofnt.html'>David Perry</a>, under the <a href='http://scripts.sil.org/OFL'/>SIL OFL 1.1</a>"
        val ta = obtainStyledAttributes(intArrayOf(android.R.attr.textColorPrimary, R.attr.colorAccent))
        val textColor = String.format("#%06X", 0xFFFFFF and ta.getColor(0, Color.BLACK))
        val accentColor = String.format("#%06X", 0xFFFFFF and ta.getColor(1, Color.BLUE))
        ta.recycle()
        about = "<style media=\"screen\" type=\"text/css\">" +
                "body {\n" +
                "    color:" + textColor + ";\n" +
                "}\n" +
                "a:link {color:" + accentColor + "}\n" +
                "</style>" +
                about
        webView.setBackgroundColor(Color.TRANSPARENT)
        webView.loadData(about, "text/html", "UTF-8")
        alert.setView(webView, 32, 0, 32, 0)
        alert.setPositiveButton(R.string.dialog_ok, DialogInterface.OnClickListener { dialog, whichButton -> })
        alert.show()
    }
}