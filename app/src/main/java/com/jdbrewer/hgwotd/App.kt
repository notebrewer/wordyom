package com.jdbrewer.hgwotd

import android.app.Application
import com.jdbrewer.hgwotd.utils.Prefs



// val prefs: Prefs by lazy {
//     App.prefs!!
// }

class App : Application() {
    companion object {
        var prefs: Prefs? = null
    }

    override fun onCreate() {
        //removed applicationContext here and in Preferences.kt
//        prefs = Prefs(applicationContext)
        prefs = Prefs()
        super.onCreate()
    }
}