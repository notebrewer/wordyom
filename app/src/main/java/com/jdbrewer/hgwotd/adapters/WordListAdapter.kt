package com.jdbrewer.hgwotd.adapters


import android.content.Context
import android.content.Intent
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jdbrewer.hgwotd.R
import com.jdbrewer.hgwotd.activities.MainActivity
import com.jdbrewer.hgwotd.models.Word



class WordListAdapter(private val wordSetArrayList: ArrayList<Word>): RecyclerView.Adapter<WordListAdapter.ViewHolder>() {

    private var mcon: Context? = null

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        holder?.wordLabel?.text = wordSetArrayList[position].Hebrew
        holder?.defLabel?.text = wordSetArrayList[position].English
        holder?.itemView?.setOnClickListener(View.OnClickListener {
            val adapPos = holder.adapterPosition
            mcon = it.context
            val go = Intent(mcon, MainActivity::class.java)
            go.putExtra("viewpager_position", adapPos)
            mcon?.startActivity(go)
        } )
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.recycler_view_item, parent, false)
        return ViewHolder(v)

    }

    override fun getItemCount(): Int {
        return wordSetArrayList.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val wordLabel = itemView.findViewById<TextView>(R.id.wordLabel)
        val defLabel = itemView.findViewById<TextView>(R.id.definitionLabel)
        val viewPager = itemView.findViewById<ViewPager>(R.id.viewPager)
    }
}